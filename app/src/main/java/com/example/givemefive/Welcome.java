package com.example.givemefive;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class Welcome extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//Set Portrait
        init();
    }

    private void init() {
        Typeface type = Typeface.createFromAsset(getAssets(),"font/Ultra-Regular.ttf");
        TextView tv = (TextView) findViewById(R.id.textView);
        TextView tv2 = (TextView) findViewById(R.id.textView2);
        TextView tv3 = (TextView) findViewById(R.id.textView3);
        tv.setTypeface(type);
        tv2.setTypeface(type);
        tv3.setTypeface(type);
        tv.setOnClickListener(this);
        tv2.setOnClickListener(this);
        tv3.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.textView2:
                Intent i = new Intent(Welcome.this, Game.class);
                startActivity(i);
                break;

            case R.id.textView3:
                Intent j = new Intent(Welcome.this, Rules.class);
                startActivity(j);
                break;

            default:
                break;
        }

    }
}