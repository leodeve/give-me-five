package com.example.givemefive;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Game extends AppCompatActivity {
    Button btnplus1, btnmoins1, btnplus2, btnmoins2, btnplus3, btnmoins3, btnplus4, btnmoins4;
    TextView txtlettre, txtlettre2, txttheme, txttheme2;
    String randomLettre, randomTheme, ancienRandomLettre, ancienRandomTheme;
    static int nbPoints1=0, nbPoints2=0, nbPoints3=0, nbPoints4=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ImageView triangle = findViewById(R.id.triangle);
        ImageView triangle2 = findViewById(R.id.triangle2);

        txtlettre = (TextView) findViewById(R.id.txtlettre);
        txtlettre2 = (TextView) findViewById(R.id.txtlettre2);
        txttheme = (TextView) findViewById(R.id.txttheme);
        txttheme2 = (TextView) findViewById(R.id.txttheme2);
        btnplus1 = (Button) findViewById(R.id.btnplus1);
        btnmoins1 = (Button) findViewById(R.id.btnmoins1);
        btnplus2 = (Button) findViewById(R.id.btnplus2);
        btnmoins2 = (Button) findViewById(R.id.btnmoins2);
        btnplus3 = (Button) findViewById(R.id.btnplus3);
        btnmoins3 = (Button) findViewById(R.id.btnmoins3);
        btnplus4 = (Button) findViewById(R.id.btnplus4);
        btnmoins4 = (Button) findViewById(R.id.btnmoins4);


        triangle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeLettre();
            }
        });
        triangle2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeLettre();
                changeTheme();
            }
        });

        btnplus1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                equipe1_plus();
            }
        });
        btnmoins1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                equipe1_moins();
            }
        });
        btnplus2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                equipe2_plus();
            }
        });
        btnmoins2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                equipe2_moins();
            }
        });
        btnplus3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                equipe3_plus();
            }
        });
        btnmoins3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                equipe3_moins();
            }
        });
        btnplus4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                equipe4_plus();
            }
        });
        btnmoins4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                equipe4_moins();
            }
        });
    }

    private String getCharForNumber(int i) {
        return i > 0 && i < 27 ? String.valueOf((char)(i + 64)) : null;
    }

    private void changeLettre() {
        List<Integer> listeNb = new ArrayList<>(); //liste de 17 nombres
        for(int i=0;i<26;i++){
            if(i!=7 && i!=8 && i!=10 && i!=16 && i!=20 && i!=22 && i!=23 && i!=24 && i!=25){ //H=8-1, I=9-1, K=11-1, Q=17-1, U=21-1, W=23-1, X, Y, Z
                listeNb.add(i);
            }
        }
        Collections.shuffle(listeNb);
        Collections.shuffle(listeNb);
        Collections.shuffle(listeNb);
        do{
            int randomNb = new Random().nextInt(listeNb.size());
            randomLettre = getCharForNumber(listeNb.get(randomNb) + 1);
        }while(randomLettre.equals(ancienRandomLettre));
        ancienRandomLettre = randomLettre;

        txtlettre.setText(randomLettre);
        txtlettre2.setText(randomLettre);
    }

    private void changeTheme() {
        String[] theme = {
                "C'est chaud", "C'est froid",
                "Ça sent bon", "Ça sent mauvais",
                "Ça coule",
                "C'est étrange",
                "C'est joli",
                "Cadeau d'anniversaire",
                "C'est grand", "C'est petit",
                "C'est bleu", "C'est rouge", "C'est vert", "C'est blanc",
                "On en trouve dans la forêt", "On en trouve dans la mer", "On en trouve dans la rue", "On en trouve dans un bar",
                "On en trouve dans une salle de bain", "On en trouve dans une voiture", //20
                "On en trouve dans un avion", "On en trouve dans un amphi",
                "Ça court", "Ça vole", "Ça se mange", "Ça se boit",
                "On en trouve surtout la nuit",
                "Thème libre",
                "C'est rond", "C'est carré",
                "Prénom", "Ville", "Pays",
                "On en trouve au supermarché", "On en trouve sur une table",
                "On en trouve sur une crêpe",
                "C'est doux", "Ça pique", "Ça gratte", "Ça se porte", "Ça empêche de dormir", "Ça réveille" //42

        };
        List<Integer> listeNb = new ArrayList<>();
        for (int i = 0; i < theme.length; i++) {
            listeNb.add(i);
        }
        Collections.shuffle(listeNb);
        Collections.shuffle(listeNb);
        Collections.shuffle(listeNb);
        do{
            int randomNb = new Random().nextInt(theme.length);
            randomTheme = theme[listeNb.get(randomNb)];
        }while(randomTheme.equals(ancienRandomTheme));
        ancienRandomTheme = randomTheme;

        txttheme.setText(randomTheme);
        txttheme2.setText(randomTheme);
    }

    private void equipe1_plus(){
        nbPoints1++;
        String strnbPoints = String.valueOf(nbPoints1);
        TextView txtscore1 = (TextView) findViewById(R.id.txtscore1);
        txtscore1.setText(strnbPoints);
    }

    private void equipe1_moins(){
        nbPoints1--;
        String strnbPoints = String.valueOf(nbPoints1);
        TextView txtscore1 = (TextView) findViewById(R.id.txtscore1);
        txtscore1.setText(strnbPoints);
    }

    private void equipe2_plus(){
        nbPoints2++;
        String strnbPoints = String.valueOf(nbPoints2);
        TextView txtscore1 = (TextView) findViewById(R.id.txtscore2);
        txtscore1.setText(strnbPoints);
    }

    private void equipe2_moins(){
        nbPoints2--;
        String strnbPoints = String.valueOf(nbPoints2);
        TextView txtscore1 = (TextView) findViewById(R.id.txtscore2);
        txtscore1.setText(strnbPoints);
    }

    private void equipe3_plus(){
        nbPoints3++;
        String strnbPoints = String.valueOf(nbPoints3);
        TextView txtscore1 = (TextView) findViewById(R.id.txtscore3);
        txtscore1.setText(strnbPoints);
    }

    private void equipe3_moins(){
        nbPoints3--;
        String strnbPoints = String.valueOf(nbPoints3);
        TextView txtscore1 = (TextView) findViewById(R.id.txtscore3);
        txtscore1.setText(strnbPoints);
    }

    private void equipe4_plus(){
        nbPoints4++;
        String strnbPoints = String.valueOf(nbPoints4);
        TextView txtscore1 = (TextView) findViewById(R.id.txtscore4);
        txtscore1.setText(strnbPoints);
    }

    private void equipe4_moins(){
        nbPoints4--;
        String strnbPoints = String.valueOf(nbPoints4);
        TextView txtscore1 = (TextView) findViewById(R.id.txtscore4);
        txtscore1.setText(strnbPoints);
    }


}
