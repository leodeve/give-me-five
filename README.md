# give-me-five

Give Me Five est un jeu de cartes existant dont j'ai réalisé la version mobile. <br/>
Il s'agit d'un jeu dans lequel il faut trouver des mots en équipes. <br/>
Ces mots doivent correspondre à un thème et doivent commencer par une certaine lettre.

Voici les différentes versions que j'ai réalisées (j'ai directement mis la version 2.1 sur git) : <br/>
**Version 1.0** : 20 lettres (sans KUWXYZ), 30 thèmes dans un switch case, nom : GiveMeFive, icône "5", comptage de points avec cacahuètes <br/>
**Version 1.1** : 18 lettres (sans HQ), 47 thèmes dans une liste, nom : Give Me Five, 4 équipes <br/>
**Version 2.0** : 17 lettres (sans I), mise à jour des thèmes, écran d'accueil, nouveau design avec un jeu en miroir <br/>
**Version 2.1** : lettres/themes changent obligatoirement, détection de l'appui sur le triangle
